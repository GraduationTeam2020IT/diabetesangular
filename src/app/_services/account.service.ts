import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { RegisterAsDoctorModel } from '../_models/RegisterAsDoctor.model';
import { RegisterAsPatientModel } from '../_models/RegisterAsPatient.model';
import {
  HttpClientModule,
  HttpClient,
  HttpHeaders,
} from '@angular/common/http';
import { LoginModel } from '../_models/Login.model';
//import { RegisterAsPatientComponent } from './register-as-patient/register-as-patient.component';
import { map } from 'rxjs/operators';

export const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
  observe: 'response',
};
@Injectable({
  providedIn: 'root',
})
export class AccountService {
  readonly rootURL = 'http://localhost:51273';
  constructor(private http: HttpClient, private router: Router) {}
  RegisterAsDoctor(regModel: RegisterAsDoctorModel) {
    return this.http.post<RegisterAsDoctorModel>(
      'http://localhost:51273/Account/RegisterAsDoctor',
      regModel
    );
  }
  RegisterAsPatient(regModel: RegisterAsPatientModel) {
    console.log(regModel);

    return this.http.post<RegisterAsPatientModel>(
      'http://localhost:51273/Account/RegisterAsPatient',
      regModel
    );
  }
  Login(logModel: LoginModel) {
    // var header_ = new Headers ({'Content-Type':'application/x-www-form-urlencoded'});
    return this.http
      .post<LoginModel>('http://localhost:51273/Account/loginn', logModel)
      .pipe(
        map((user) => {
          if (user) {
            sessionStorage.setItem('TokenInfo', JSON.stringify(user));
          }
        })
      );
  }
  loggedin() {
    return !!sessionStorage.getItem('TokenInfo');
  }
  getToken() {
    return sessionStorage.getItem('TokenInfo');
  }
  logout() {
    sessionStorage.removeItem('TokenInfo');
    this.router.navigate(['/home']);
  }
}
