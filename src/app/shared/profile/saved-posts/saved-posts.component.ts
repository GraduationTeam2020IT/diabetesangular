import { Component, OnInit } from '@angular/core';
import { PostModel } from '../../../_models/Posts.model';
import { PostService } from '../../../_Services/post.service';
import { CategoriesModel } from '../../../_models/Categories.model';
import { ReactionModel } from '../../../_models/Reactions.model';
import { CategoryService } from '../../../_Services/category.service';
import { ReactionService } from '../../../_Services/reaction.service';
import { TokenService } from '../../../_Services/token.service';

@Component({
  selector: 'app-saved-posts',
  templateUrl: './../myposts/myposts.component.html',
  styleUrls: ['././../myposts/myposts.component.css'],
})
export class SavedPostsComponent implements OnInit {
  p: PostModel = new PostModel();
  p_arr: PostModel[] = [];
  ps_arr: PostModel[] = [];
  c: CategoriesModel[] = [];
  r: ReactionModel[] = [];
  userName: string;
  constructor(
    private c_serv: CategoryService,
    private token: TokenService,
    private r_ser: ReactionService,
    private p_serv: PostService
  ) {}

  ngOnInit(): void {
    this.c_serv.getCategory().subscribe((a) => {
      this.c = a;
    });
    this.r_ser.getReactions().subscribe((a) => {
      this.r = a;
    });
    this.userName = this.token.getUserName();
    this.p_serv.getSavedPosts().subscribe((a) => {
      this.p_arr = a;
      console.log(a);
    });
  }
}
