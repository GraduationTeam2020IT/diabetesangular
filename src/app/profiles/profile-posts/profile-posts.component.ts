import { Component, OnInit } from '@angular/core';
import { PostModel } from '../../_models/Posts.model';
import { QuestionModel } from '../../_models/Question.model';
import { QuestionService } from '../../_Services/Question.services';
import { PostService } from '../../_Services/post.service';
import { CategoriesModel } from '../../_models/Categories.model';
import { ReactionModel } from '../../_models/Reactions.model';
import { CategoryService } from '../../_Services/category.service';
import { ReactionService } from '../../_Services/reaction.service';
import { TokenService } from '../../_Services/token.service';
import { DoctorService } from 'src/app/_Services/doctor.service';

@Component({
  selector: 'app-profile-posts',
  templateUrl: '../../shared/profile/myposts/myposts.component.html',
  styleUrls: ['../../shared/profile/myposts/myposts.component.css'],
})
export class ProfilePostsComponent implements OnInit {
  p: any;
  p_arr: any;
  ps_arr: PostModel[] = [];
  c: CategoriesModel[] = [];
  r: ReactionModel[] = [];
  q_arr: QuestionModel[] = [];
  userName: string;
  id = 8;
  constructor(private drserv: DoctorService) {}

  ngOnInit(): void {
    this.drserv.getdocpostsForUser(this.id).subscribe((a) => {
      this.p_arr = a;
      console.log(a);
      console.log(this.p_arr);
    });
  }
}
