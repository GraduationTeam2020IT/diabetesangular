import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import {
  NgbPaginationModule,
  NgbAlertModule,
} from '@ng-bootstrap/ng-bootstrap';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { SelectDropDownModule } from 'ngx-select-dropdown';
import { ProfileComponent } from './profile/profile.component';
import { SquaresBgComponent } from './squares-bg/squares-bg.component';
import { MypostsComponent } from './profile/myposts/myposts.component';
import { QuestionsComponent } from './questions/questions.component';
import { AddAnswerComponent } from './add-answer/add-answer.component';
// import { MatSelectModule } from '@angular/material/select';
// import { MatFormFieldModule } from '@angular/material/form-field';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';

import { MatInputModule } from '@angular/material/input';
import { SavedPostsComponent } from './profile/saved-posts/saved-posts.component';
import { EditProfileComponent } from './profile/edit-profile/edit-profile.component';
import { DialogComponent } from './dialog/dialog.component';
@NgModule({
  declarations: [
    HomeComponent,
    ProfileComponent,
    SquaresBgComponent,
    MypostsComponent,
    QuestionsComponent,
    AddAnswerComponent,
    SavedPostsComponent,
    EditProfileComponent,
    DialogComponent,
  ],
  imports: [
    NgbPaginationModule,
    NgbAlertModule,
    TabsModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgbPaginationModule,
    NgbAlertModule,
    SelectDropDownModule,
    MatCardModule,
    MatFormFieldModule,
    MatTabsModule,
    MatDividerModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatListModule,
    MatTableModule,
    MatInputModule,
  ],
  exports: [
    HomeComponent,
    ProfileComponent,
    SquaresBgComponent,
    MypostsComponent,
    QuestionsComponent,
    AddAnswerComponent,
  ],
})
export class SharedModule {}
