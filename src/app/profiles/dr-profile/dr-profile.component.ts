import { Component, OnInit } from '@angular/core';
import { PostModel } from '../../_models/Posts.model';
import { QuestionModel } from '../../_models/Question.model';
import { QuestionService } from '../../_Services/Question.services';
import { PostService } from '../../_Services/post.service';
import { CategoriesModel } from '../../_models/Categories.model';
import { ReactionModel } from '../../_models/Reactions.model';
import { CategoryService } from '../../_Services/category.service';
import { ReactionService } from '../../_Services/reaction.service';
import { TokenService } from '../../_Services/token.service';
import { DoctorService } from 'src/app/_Services/doctor.service';

@Component({
  selector: 'app-dr-profile',
  templateUrl: './dr-profile.component.html',
  styleUrls: ['./dr-profile.component.css'],
})
export class DrProfileComponent implements OnInit {
  public token: TokenService;
  public id: number;
  public doctor: any;
  public profile: any;
  public cr: any;
  flag: boolean; //ture doctor //false patient
  constructor(public serv: DoctorService) {}

  ngOnInit(): void {
    this.id = 8;

    this.serv.getdocinfoForUser(this.id).subscribe((a) => {
      this.profile = a;
      console.log(a);
    });
    document.getElementById('my_p').hidden = false;
    document.getElementById('my_q').hidden = true;
  }

  showMyPosts() {
    document.getElementById('my_p').hidden = false;
    document.getElementById('my_q').hidden = true;
    document.getElementById('my_ps').hidden = true;
    // document.getElementById('my_pw').hidden = true;
  }
  showMyQuestions() {
    document.getElementById('my_p').hidden = true;
    document.getElementById('my_q').hidden = false;
    document.getElementById('my_ps').hidden = true;
  }
}
