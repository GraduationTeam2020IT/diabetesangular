import { Component, OnInit } from '@angular/core';
import { DoctorService } from '../../_Services/doctor.service';
import { MatDialog } from '@angular/material/dialog';
import { AnswerModel } from '../../_Models/answer-model';
import { TokenService } from '../../_Services/token.service';
import { QuestionService } from 'src/app/_Services/Question.services';
import { AddAnswerComponent } from '../add-answer/add-answer.component';

export interface DialogData {
  Answer: string;
}
@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css'],
})
export class QuestionsComponent implements OnInit {
  questions: any;
  Answer: string;
  Ans: AnswerModel = new AnswerModel();
  filter: string[] = [];

  constructor(
    public serv: DoctorService,
    public dialog: MatDialog,
    public tokserv: TokenService,
    public Qserv: QuestionService
  ) {}

  getdocans(id: number, i: number) {
    this.filter[i] = 'أجابات الاطباء';
    this.Qserv.getDocAns(id).subscribe((a) => {
      console.log(a);
      this.questions[i].answers = a;
    });
  }
  getuserans(id: number, i: number) {
    this.filter[i] = 'أجابات المستخدمين';
    this.Qserv.getUserAns(id).subscribe((a) => {
      console.log(a);
      this.questions[i].answers = a;
    });
  }
  getallanss(id: number, i: number) {
    this.filter[i] = 'جميع الأجابات';
    this.Qserv.getAllAns(id).subscribe((a) => {
      console.log(a);
      this.questions[i].answers = a;
    });
  }
  noans(i: number) {
    this.filter[i] = 'بدون اجابات';
    this.questions[i].answers = [];
  }
  openDialog(id: number, i: number): void {
    const dialogRef = this.dialog.open(AddAnswerComponent, {
      width: '250px',
      data: { Answer: this.Answer },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      if (result != undefined) {
        this.Answer = result;
        this.serv.Addanswer(result, id).subscribe((a) => {
          console.log(a);
          if (this.tokserv.isPatient()) {
            this.getuserans(id, i);
          } else this.getdocans(id, i);

          this.questions[i].answers.push({
            answer: a.answer,
            date: a.date,
            userName: this.tokserv.getUserName(),
          });
        });
      }
    });
  }
  ngOnInit(): void {
    this.Qserv.getmyQuestions().subscribe((a) => {
      this.questions = a;
      this.filter = new Array((this.questions as Array<any>).length);
      this.filter.fill('أجابات الاطباء');
      console.log(this.questions);
      console.log(this.filter);
    });
  }
}
