import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { RegisterAsDoctorModel } from '../../_models/RegisterAsDoctor.model';
import { AccountService } from '../../_services/account.service';
@Component({
  selector: 'app-register',
  templateUrl: './register-as-doctor.component.html',
  styleUrls: ['./register-as-doctor.component.css'],
})
export class RegisterAsDoctorComponent implements OnInit, OnDestroy {
  regform: FormGroup;
  regmodel: RegisterAsDoctorModel = new RegisterAsDoctorModel();
  focus;
  focus1;
  focus2;
  focus3;
  focus4;
  focus5;

  constructor(private formbuilder: FormBuilder, private serv: AccountService) {}

  registerAsDoctor() {
    this.assigntoobj();
    this.serv.RegisterAsDoctor(this.regmodel).subscribe(
      (succes) => {
        alert('Registration succes');
      },
      (err) => console.log(err)
    );
  }

  assigntoobj() {
    this.regmodel.Email = this.regform.value.Email;
    this.regmodel.UserName = this.regform.value.UserName;
    this.regmodel.Password = this.regform.value.Password;
    this.regmodel.Address = this.regform.value.Address;
    this.regmodel.PhoneNumber = this.regform.value.PhoneNumber;
  }
  IsPassMatch() {
    if (
      this.regform.value.ConfirmPassword !== '' &&
      this.regform.value.Password !== ''
    ) {
      if (this.regform.value.Password !== this.regform.value.ConfirmPassword) {
        return true;
      }
    }

    return false;
  }
  ngOnInit(): void {
    this.regform = this.formbuilder.group({
      UserName: ['', Validators.required],
      Email: ['', [Validators.required, Validators.email]],
      Password: ['', [Validators.required, Validators.minLength(6)]],
      ConfirmPassword: ['', [Validators.required]],
      PhoneNumber: ['', Validators.required],
      Address: ['', Validators.required],
    });
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('register-page');
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('register-page');
  }

  // OnSubmit(form : NgForm){

  // }
}
