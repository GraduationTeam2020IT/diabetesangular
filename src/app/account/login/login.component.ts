import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { AccountService } from '../../_services/account.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginModel } from '../../_models/Login.model';
import noUiSlider from 'nouislider';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, OnDestroy {
  logform: FormGroup;
  logmodel: LoginModel = new LoginModel();
  isCollapsed = true;
  focus;
  focus1;
  focus2;

  constructor(private formbuilder: FormBuilder, private serv: AccountService) {}

  login() {
    this.assigntoobj();
    this.serv.Login(this.logmodel).subscribe(
      (succes) => {
        alert('login succes'), console.log(succes);
      },
      (err) => console.log(err)
    );
  }
  assigntoobj() {
    this.logmodel.Email = this.logform.value.Email;
    this.logmodel.Password = this.logform.value.Password;
    this.logmodel.RememberMe = this.logform.value.RememberMe;
  }
  ngOnInit(): void {
    this.logform = this.formbuilder.group({
      Email: ['', Validators.required],
      Password: ['', [Validators.required, Validators.minLength(6)]],
      RememberMe: [false],
    });
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('register-page');
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('register-page');
  }
}
