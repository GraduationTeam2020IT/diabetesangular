import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PatientProfile } from '../../../_models/patient-profile.model';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { patientEditModel } from 'src/app/_Models/PatienttEditModel';
import { PatientEditProfileService } from 'src/app/_services/patient-edit-profile.service';
import { DialogComponent } from '../../dialog/dialog.component';

export interface DialogData {
  patient: patientEditModel;
  key: string;
  value: string;
}
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss', './edit-profile.component.css'],
  encapsulation: ViewEncapsulation.ShadowDom,
})
export class EditProfileComponent implements OnInit {
  patientprofile: patientEditModel = new patientEditModel();
  age: number;
  id: number = 47;
  flag: boolean = false;
  constructor(
    private p: PatientEditProfileService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog
  ) {}
  // openDialog(): void {
  //   const dialogRef = this.dialog.open(DialogComponent, {
  //     width: '250px',
  //     data: { PhoneNumber: this.PhoneNumber },
  //   });

  //   dialogRef.afterClosed().subscribe((result) => {
  //     console.log(result);
  //     if (result != undefined) {
  //       this.PhoneNumber = result;
  //       this.serv.updatephone(result).subscribe((a) => console.log(a));
  //     }
  //   });
  // }
  openDialog2() {}
  openDialog(key: string, value: string) {
    this.flag = true;
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: { value: key, key: value, patient: this.patientprofile },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      if (result != undefined) {
        //  this.PhoneNumber=result;
        // this.doctor.phoneNumber = result;
        // this.serv.updatephone(result).subscribe((a) => console.log(a));
        console.log(this.patientprofile);
      }
    });
  }
  ngOnInit(): void {
    // this.route.snapshot.paramMap.get('id');
    console.log('the id equals ', this.id);
    if (this.id) {
      this.p.GetMyProfile().subscribe((a) => {
        this.patientprofile = a;
        console.log(this.patientprofile);
        this.CalculateAge();
        console.log(this.age);
      });
    }
  }
  Submit() {
    this.flag = false;
    this.p.UpdateMyProfile(this.patientprofile).subscribe((a) => {
      console.log(a);
    });
  }
  public CalculateAge(): void {
    {
      var timeDiff = Math.abs(
        Date.now() - new Date(this.patientprofile.birth_date).getTime()
      );
      this.age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
    }
  }
}
