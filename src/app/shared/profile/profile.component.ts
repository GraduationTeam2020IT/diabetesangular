import { Component, OnInit } from '@angular/core';
import { PostModel } from '../../_models/Posts.model';
import { QuestionModel } from '../../_models/Question.model';
import { QuestionService } from '../../_Services/Question.services';
import { PostService } from '../../_Services/post.service';
import { CategoriesModel } from '../../_models/Categories.model';
import { ReactionModel } from '../../_models/Reactions.model';
import { CategoryService } from '../../_Services/category.service';
import { ReactionService } from '../../_Services/reaction.service';
import { TokenService } from '../../_Services/token.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  p: PostModel = new PostModel();
  p_arr: PostModel[] = [];
  ps_arr: PostModel[] = [];
  c: CategoriesModel[] = [];
  r: ReactionModel[] = [];
  q_arr: QuestionModel[] = [];
  userName: string;
  email: string;
  role: string;
  constructor(
    private c_serv: CategoryService,
    private token: TokenService,
    private r_ser: ReactionService,
    private p_serv: PostService
  ) //  private q_serv: QuestionService
  {}

  ngOnInit(): void {
    document.getElementById('my_p').hidden = false;
    document.getElementById('my_q').hidden = true;
    document.getElementById('my_ps').hidden = true;
    this.c_serv.getCategory().subscribe((a) => {
      this.c = a;
    });
    this.r_ser.getReactions().subscribe((a) => {
      this.r = a;
    });
    this.p_serv.getmyPosts().subscribe((a) => {
      this.p_arr = a;
      console.log(a);
    });
    // this.q_serv.getmyQuestions().subscribe((a) => {
    //   this.q_arr = a;
    //   console.log(a);
    // });
    this.p_serv.getSavedPosts().subscribe((a) => {
      this.ps_arr = a;
      console.log(a);
    });

    this.userName = this.token.getUserName();
    this.email = this.token.gettoken().email;
    this.role = this.token.gettoken().role;
    console.log(this.userName);
  }
  add() {
    this.p_serv.addPost(this.p).subscribe((a) => {
      console.log(a);
    });
    console.log(this.p);
  }

  showMyPosts() {
    document.getElementById('my_p').hidden = false;
    document.getElementById('my_q').hidden = true;
    document.getElementById('my_ps').hidden = true;
    // document.getElementById('my_pw').hidden = true;
  }
  showMyQuestions() {
    document.getElementById('my_p').hidden = true;
    document.getElementById('my_q').hidden = false;
    document.getElementById('my_ps').hidden = true;
    // document.getElementById('my_pw').hidden = true;
  }
  showMySavedPosts() {
    document.getElementById('my_p').hidden = true;
    document.getElementById('my_q').hidden = true;
    document.getElementById('my_ps').hidden = false;
    //  document.getElementById('my_pw').hidden = true;
  }
  addpost() {
    document.getElementById('my_p').hidden = true;
    document.getElementById('my_q').hidden = true;
    document.getElementById('my_ps').hidden = true;
    // document.getElementById('my_pw').hidden = false;
  }
}
