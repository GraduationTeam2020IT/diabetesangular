import { Component, OnInit } from '@angular/core';
import { PostModel } from '../../_models/Posts.model';
import { CategoriesModel } from '../../_models/Categories.model';
import { ReactionModel } from '../../_models/Reactions.model';
import { CategoryService } from '../../_Services/category.service';
import { PostService } from '../../_Services/post.service';
import { ReactionService } from '../../_Services/reaction.service';
import { of, concat } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  p: PostModel = new PostModel();
  // p_arr: PostModel[] = [];
  p_arr: any;
  c: CategoriesModel[] = [];
  r: ReactionModel[] = [];
  ctg_ids: any[];
  file_img: File = null;
  img_url: String = '/assets/img/';

  constructor(
    private c_serv: CategoryService,
    private r_ser: ReactionService,
    private p_serv: PostService
  ) {}

  ngOnInit(): void {
    this.ctg_ids = [];
    this.c_serv.getCategory().subscribe((a) => {
      this.c = a;
    });
    this.r_ser.getReactions().subscribe((a) => {
      this.r = a;
    });
    this.p_serv.getRandomPosts().subscribe((a) => {
      this.p_arr = a;
      console.log(a);
    });
  }
  add() {
    this.p_serv.addPost(this.p).subscribe((a) => {
      console.log(a);
    });
    console.log(this.p);
  }
  filterposts() {
    if (this.ctg_ids.length < 1) {
      this.p_serv.getRandomPosts().subscribe((a) => {
        this.p_arr = a;
        console.log(a);
      });
    } else {
      this.p_serv.getPosts(-1, this.ctg_ids).subscribe((a) => {
        this.p_arr = a;
      });
    }
    console.log(this.ctg_ids);
  }
  getLastPostID() {
    var cd = document.getElementsByName('post_card');
    var id = cd[cd.length - 1].firstElementChild.innerHTML;
    return parseInt(id);
  }
  showMore() {
    if (this.ctg_ids.length < 1) {
      //this.p_serv.getRandomPosts().subscribe(a=>{this.p_arr=a;})
      //this.p_serv.getPosts(this.getLastPostID(),[1,2,3,4,5,6,7,8,9,10]).subscribe(a=>{this.p_arr=a;})

      concat(
        of(
          this.p_serv.getRandomPosts().subscribe((a) => {
            this.p_arr = a;
          })
        ),
        of(
          this.p_serv
            .getPosts(this.getLastPostID(), [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
            .subscribe((a) => {
              this.p_arr = a;
              console.log(a);
            })
        )
      ).subscribe(console.log);
    } else {
      this.p_serv
        .getPosts(this.getLastPostID(), this.ctg_ids)
        .subscribe((a) => {
          this.p_arr = a;
        });
    }
  }
  clickItem(e) {
    if (e.style.background == 'lightgrey') {
      var i = this.ctg_ids.indexOf(e.value);
      e.style.background = 'transparent';
      this.ctg_ids.splice(i, 1);
    } else {
      this.ctg_ids.push(e.value);
      e.style.background = 'lightgrey';
    }
  }

  // selectFile(event){
  //   if (event.target.files && event.target.files[0]) {

  //     var reader = new FileReader();
  //     reader.readAsDataURL(event.target.files[0]);// read file as data url

  //    var str= event.target.value.replace("C:\\fakepath\\", "");
  //     console.log(event.target.result);

  //     reader.onload = (event:any) => { // called once readAsDataURL is completed
  //       this.img_url = event.target.result;
  //     }

  //   }
  // }
}
