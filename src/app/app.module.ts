import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccountModule } from './account/account.module';
import { PagesModule } from './pages/pages.module';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from './components/components.module';
import { ExamplesModule } from './examples/examples.module';
// import { NavbarComponent } from './shared/navbar/navbar.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
// import { NavbarComponent } from './navbar/navbar.component';
import { NavbarModule } from './navbar/navbar.module';
import { SharedModule } from './shared/shared.module';
import { ErrorInterceptor } from './_Interceptor/error_Interceptor';
import { TokenInterceptor } from './_Interceptor/jwt.interceptor';
import { DoctorModule } from './doctor/doctor.module';
import { ProfilesModule } from './profiles/profiles.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    PagesModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AccountModule,
    HttpClientModule,
    ComponentsModule,
    ExamplesModule,
    FormsModule,
    NgbModule,
    RouterModule,
    NavbarModule,
    SharedModule,
    DoctorModule,
    ProfilesModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
